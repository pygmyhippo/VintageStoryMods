using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Server;

[assembly: ModInfo( "Lovecraftians",
	Description = "Adds lovecraftian creatures",
	Website     = "https://gitea.com/pygmyhippo/VintageStoryMods",
	Authors     = new []{ "pygmyhippo" } )]

namespace Lovecraftians
{
	public class LovecraftiansMod : ModSystem
	{
		public override void Start(ICoreAPI api)
		{

		}
		
		public override void StartClientSide(ICoreClientAPI api)
		{
			
		}
		
		public override void StartServerSide(ICoreServerAPI api)
		{
			
		}
	}
}