#!/usr/bin/sh

if ! [ -x "$(command -v 7z)" ]; then
    echo "Could not find 7z"
    exit 127
fi

for mod in ../*; do
    if [ -d "$mod/resources" ]; then
        mod_name=${mod##*/}
        archive_name="$VINTAGE_STORY_DATA/Mods/$mod_name.zip"

        if [ -f "$archive_name" ]; then
            rm "$archive_name"
        fi

        7z a "$archive_name" "$mod/resources/*" > /dev/null

        if [ -d "$mod/src" ]; then
            project_path=$(find "$mod" -type f -name "*.csproj")
            project_file=${project_path##*/}
            lib_name=${project_file%.*}
            lib_file="$mod/bin/Release/net452/$lib_name.dll"
            
            if [ -f "$lib_file" ]; then
                7z a "$archive_name" "$lib_file" > /dev/null
            fi
        fi
    fi
done
